#!/usr/bin/bash

# handy for dev - put this in terminal?
# while inotifywait -e modify get-gitlog.sh; do bash get-gitlog.sh; done

cd /home/jas/code/nx-workspace

IFS='|' 
echo "["
while read name timestamp date comment; do
    firstname=$(echo $name | sed 's/\([^[:blank:]]*\).*/\1/')
    firstname=$(echo $firstname | sed 's/\([^\.]*\).*/\L\1/')
    firstname=$(echo $firstname | sed 's/water//')
    # firstname=$(echo $firstname | sed 's/[WATER|\.]//g')
    printf "{\"name\": \"%s\", \"week\": \"%(%W)T\", \"day\": \"%(%u)T\", \"hour\": \"%(%H)T\", \"timestamp\": \"%s\", \"date\": \"%s\" , \"comment\": \"%s\"},\n" "$firstname" "$timestamp" "$timestamp" "$timestamp" "$timestamp" "$date" "$comment"
done < <(git log --all --since=6.months.ago --format='%an|%ct|%cI|%s')
echo "]"