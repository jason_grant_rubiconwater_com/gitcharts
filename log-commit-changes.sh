#!/usr/bin/bash

# handy for dev - put this in terminal?
# while inotifywait -e modify get-gitlogv2.sh; do bash get-gitlogv2.sh; done

cd /home/jas/code/nx-workspace

IFS='|' 
echo "["
while read name timestamp changed added deleted; do
    firstname=$(echo $name | sed 's/\([^[:blank:]]*\).*/\1/')
    firstname=$(echo $firstname | sed 's/\([^\.]*\).*/\L\1/')
    firstname=$(echo $firstname | sed 's/water//')
    # firstname=$(echo $firstname | sed 's/[WATER|\.]//g')
    printf "{\"name\": \"%s\", \"changed\": \"%s\", \"added\": \"%s\", \"deleted\": \"%s\", \"week\": \"%(%W)T\", \"day\": \"%(%u)T\", \"hour\": \"%(%H)T\"},\n" "$firstname" $changed $added $deleted "$timestamp" "$timestamp" "$timestamp" 
done < <(
    git --no-pager log --all --no-merges --since=6q.months.ago --format='@%an|%ct|' --shortstat |  tr "\n" " "  |  tr "@" "\n" \
    | fgrep changed \
    | sed -E 's/(.*)\|\s*([0-9]*)\s*file.*changed(.*)/\1|\2|\3/' \
    | sed -E '/insertion/!{s/(.*)\|(.*)/\1|0|\2/}; s/(.*)\|,\s*([0-9]*).*insertions?...(.*)/\1|\2|\3/' \
    | sed -E 's/(.*)\s+/\1/' \
    | sed -E '/deletion/!{s/(.*)/\10|/}; s/(.*)\|,\s*([0-9]*).*deletion(.*)/\1|\2|/' 
)

echo "]"


