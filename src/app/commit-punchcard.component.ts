import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';

@Component({
  selector: 'app-commit-punchcard',
  templateUrl: './commit-punchcard.component.html',
  styleUrls: ['./commit-punchcard.component.scss']
})
export class CommitPunchcardComponent implements OnInit, AfterViewInit {

  echartsInstance;
  chartOption: EChartsOption;

  @Input() committer: string;
  @Input() commits: Map<string, []>;
  @Input() field: number;
  @Input() bubblescale: number;
  userCommits:[];

  hours = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', ];
  days = [ 'M', 'T', 'W', 'T', 'F', 'S', 'S' ];
  title: string;
  legend: string;

  constructor() { }

  ngOnInit(): void {
    this.userCommits=this.commits.get(this.committer);
    this.title=this.committer;
    this.legend=this.committer;
    this.init();
  }

  ngAfterViewInit(): void {
  }

  init() {
    var that=this;
    this.chartOption = {
      title: {
        text: this.title,
        textAlign: "left",
        textVerticalAlign: "top",
        itemGap: 1,
        top: 10,
        left: 70
      },
      // legend: {
      //   data: [this.legend],
      //   left: 'right',
      // },
      grid: {
        left: 2,
        bottom: 10,
        right: 10,
        top: 10,
        containLabel: true,
      },
      xAxis: {
        type: 'category',
        data: this.hours,
        boundaryGap: false,
        splitLine: {
          show: true,
        },
        axisLine: {
          show: false,
        },
      },
      yAxis: {
        type: 'category',
        data: this.days,
        axisLine: {
          show: false,
        },

      },
      series: [
        {
          name: this.legend,
          type: 'scatter',
          symbolSize: function (val) {
            return val[that.field] * that.bubblescale;
          },
          data: this.userCommits,
          animationDelay: function (idx) {
            return idx * 5;
          },

        },
      ],
    };
  }

  onChartInit(ec) {
    this.echartsInstance = ec;
  }

}
