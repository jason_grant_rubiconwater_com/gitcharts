import { Injectable, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * Store the detailed commit fields against the hour
 *
 * @export
 * @class HourCommits
 */
export class Aggregates {
  count: number=0;
  changed: number=0;
  added: number=0;
  deleted: number=0;

  add(commit: any) {
    this.count++;
    this.changed+=+commit.changed;
    this.added+=+commit.added;
    // if (this.added>20) {this.added=20;}
    this.deleted+=+commit.deleted;
  }
}

/**
 * Store commit details against each gour
 *
 * @export
 * @class DayCommits
 */
export class HourCommits {
  hourMap = new Map();

  add(commit: any) {
    var aggregates: Aggregates=this.hourMap.get(+commit.hour);
    if (!aggregates) {
      aggregates = new Aggregates();
      this.hourMap.set(+commit.hour, aggregates);
    }
    aggregates.add(commit);
  }
}

/**
 * Store commit details against each day
 *
 * @export
 * @class WeekCommits
 */
export class TimeTotals {
  dayMap = new Map();
  weekMap = new Map();

  add(commit: any) {
    var hourCommits: HourCommits=this.dayMap.get(+commit.day);
    if (!hourCommits) {
      hourCommits = new HourCommits();
      this.dayMap.set(+commit.day, hourCommits);
    }
    hourCommits.add(commit);
    var aggregates: Aggregates=this.weekMap.get(+commit.week);
    if (!aggregates) {
      aggregates = new Aggregates();
      this.weekMap.set(+commit.week, aggregates);
    }
    aggregates.add(commit);
  }
}

/**
 * Store commit details against each user
 *
 * @export
 * @class UserCommits
 */
export class UserCommits {
  committers = new Map();

  /**
   * Add a commit into the cache
   *
   * @param commit
   */
  add(commit: any) {
    var timeTotals: TimeTotals=this.committers.get(commit.name);
    if (!timeTotals) {
      timeTotals=new TimeTotals();
      this.committers.set(commit.name, timeTotals);
    }
    timeTotals.add(commit);
  }

  /**
   * Flatten the commit data out into an array for each user
   */
  getHourlyStats(): Map<string, []> {
    var coordinates = new Map();
    for (let [committer,  timeTotals] of this.committers) {
      var committerData=[];
      for (let [day,  hourCommits] of timeTotals.dayMap) {
        for (let [hour,  hourAggregates] of hourCommits.hourMap) {
          var returnArray: number[] = [+hour, day-1, +hourAggregates.count, +hourAggregates.changed, +hourAggregates.added, +hourAggregates.deleted]; // coerce zero padding
          committerData.push(returnArray);
          // console.log(`${committer} ${day} ${hour} ${hours.count}`);
        }
      }
      coordinates.set(committer, committerData);
    }
    return coordinates;
  }

  /**
   * Flatten the commit data out into an array for each user
   */
   getWeeklyStatsold(): Map<string, []> {
    var coordinates = new Map();
    for (let [committer,  timeTotals] of this.committers) {
      var committerData=[];
      for (let [week,  weekAggregates] of timeTotals.weekMap) {
          var returnArray: number[] = [+week, +weekAggregates.count, +weekAggregates.changed, +weekAggregates.added, +weekAggregates.deleted]; // coerce zero padding
          committerData.push(returnArray);
      }
      coordinates.set(committer, committerData);
    }
    return coordinates;
  }

  initData(): number[][][] {
    var outer: number[][][] = new Array<Array<Array<number>>>(4);
    for (let stat = 0; stat < 4; stat++) {
      outer[stat]=new Array(52);
      for (let w = 0; w < 52; w++) {
        outer[stat][w]=[w, 0];
      }
    }
    return outer;
  }

  getWeeklyStats(): Map<string, [][][]> {
    var coordinates = new Map();
    for (let [committer,  timeTotals] of this.committers) {
      var committerData: number[][][] = this.initData();
      for (let [week,  weekAggregates] of timeTotals.weekMap) {
        //[+week, , +weekAggregates.changed, +weekAggregates.added, +weekAggregates.deleted]; 
          committerData[0][week]=[+week, +weekAggregates.count];
          committerData[1][week]=[+week, +weekAggregates.changed];
          committerData[2][week]=[+week, +weekAggregates.added];
          committerData[3][week]=[+week, +weekAggregates.deleted];
      }
      coordinates.set(committer, committerData);
    }
    return coordinates;
  }
}

@Injectable({
  providedIn: 'root'
})
export class CommitsService {

    public commits: UserCommits = new UserCommits();
  
    /**
     * Use Http
     * @param http
     */
    constructor(private http: HttpClient) {
    }
  
    /**
     * Process the data
     *
     */
    accumulate(response) {
      response.forEach(commit => {
        this.commits.add(commit);
      });
    }

    flattenHourlyStats(): Map<string, []> {
      return this.commits.getHourlyStats();
    }

    flattenWeeklyStats(): Map<string, [][][]> {
      return this.commits.getWeeklyStats();
    }
  
    /**
     * Load the data
     *
     * @return {*}
     */
    load() {
      const jsonFile = `assets/commits.json`;
      const that=this;
  
      return new Promise<void>((resolve, reject) => {
        this.http.get(jsonFile).toPromise().then((response) => {
          that.accumulate(response);
          // that.unpack();
          resolve();
        }).catch((response: any) => {
          reject(`Error reading file '${jsonFile}': ${JSON.stringify(response)}`);
        });
      });
    }
  }