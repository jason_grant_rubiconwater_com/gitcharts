import { Component, OnInit, SystemJsNgModuleLoader } from '@angular/core';
import { EChartsOption } from 'echarts';
import { CommitsService, TimeTotals } from './commits.service';
import { TimelineService } from './timeline.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  title = 'echartApp';

  /**
   * The users who did commits
   */
  committers: IterableIterator<string>;

  /**
   * Our state that contains commit timings
   */
  hourlyStats: Map<string, []>;
  weeklyStats: Map<string, [][][]>;
  commits: any[];

  max: number[] = [0,0,0,0];

  /**
   * Flag to convey when data has arrived
   */
  dataLoaded: Promise<boolean>;
  timelineDataLoaded: Promise<boolean>;

  /**
   * Creates an instance of AppComponent.
   * @param commitService
   */
  constructor(
    private commitService: CommitsService,
    private timelineService: TimelineService
    ) {
    this.commitService.load().then(() => {
      this.hourlyStats=this.commitService.flattenHourlyStats();
      this.weeklyStats=this.commitService.flattenWeeklyStats();

      // Calc max
      for (let [committer,  stats] of this.weeklyStats) {
        for (let stat = 0; stat < stats.length; stat++) {
          var series: number[][]=stats[stat];
          for (let week = 0; week < series.length; week++) {
            const value = series[week][1];
            if (+value > this.max[stat]) {
              this.max[stat] = value;
            }
          }
        }
      }
      
      this.committers=this.hourlyStats.keys();
      this.dataLoaded=Promise.resolve(true);
    });

    this.timelineService.load().then(() => {
      this.commits=this.timelineService.commits;
      this.timelineDataLoaded=Promise.resolve(true);
    });
  }

  /**
   * Init
   */
  ngOnInit(): void {
  }
}
