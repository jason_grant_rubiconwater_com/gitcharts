import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitPunchcardComponent } from './commit-punchcard.component';

describe('CommitPunchcardComponent', () => {
  let component: CommitPunchcardComponent;
  let fixture: ComponentFixture<CommitPunchcardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommitPunchcardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitPunchcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
