import { Component, Input, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';

// prettier-ignore
const hours = [
  '12a', '1a', '2a', '3a', '4a', '5a', '6a',
  '7a', '8a', '9a', '10a', '11a',
  '12p', '1p', '2p', '3p', '4p', '5p',
  '6p', '7p', '8p', '9p', '10p', '11p'
];
// prettier-ignore
const days = [
  'Not used', 'Monday', 'Tuesday', 'Wednesday',
  'Thursday', 'Friday', 'Saturday', 'Sunday'
];


function timestampAxisLabelFormatter(milliseconds: string, index): string {
  console.log(`Time: ${milliseconds}`);
  const timestamp = new Date(milliseconds);
  return timestamp.getDate() + "/" + (timestamp.getMonth()+1);
}

@Component({
  selector: 'app-commit-timeline',
  templateUrl: './commit-timeline.component.html',
  styleUrls: ['./commit-timeline.component.scss']
})
export class CommitTimelineComponent implements OnInit {

  echartsInstance;
  chartOption: EChartsOption;

  @Input() commits: [];
  @Input() field: number;
  @Input() max: number;

  data: number[];

  constructor() { }

  ngOnInit(): void {
    this.data=this.commits;
    this.init();
  }

  ngAfterViewInit(): void {
  }

  init() {
    var that=this;
    this.chartOption = {
      tooltip: {
        formatter: function (params) {
          return (
            params.value['timestamp'] +
            ' commits in ' +
            hours[params.value[0]] +
            ' of ' +
            days[params.value[0]]
          );
        }
      },
      xAxis: {
        type: 'time',
        min: 1693793661000,
        // max: '1699948110000',
        axisLabel: {
          show: true,
          formatter: timestampAxisLabelFormatter
        },
        // data: hours,
        splitLine: {
          show: true
        },
        axisLine: {
          show: false
        }
      },
      yAxis: {
        type: 'value',
        // data: days,
        axisLine: {
          show: false
        },
        axisLabel: {
          rotate: 45
        }
      },
      dataset: {
        source: this.data,
        dimensions: ['name', 'week', 'day', 'hour', 'timestamp', 'date', 'comment'],
      },
      series: [
        {
          name: 'Punch Card',
          type: 'scatter',
          symbolSize: 8,
          animationDelay: function (idx) {
            return idx * 5;
          },
          datasetIndex: 0,
          dimensions: ['timestamp', 'hour'],
          encode: {
            x: 'timestamp',
            y: 'hour',
          },
          label: {
            show: true,
            position: 'right',
            fontSize: 9,
            rotate: 40,
            formatter: (params) => { return params.value['comment'];} 
          },
        }
      ]
    };
  }

  onChartInit(ec) {
    this.echartsInstance = ec;
  }
}
