import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitLinesOfCodeComponent } from './commit-lines-of-code.component';

describe('CommitLinesOfCodeComponent', () => {
  let component: CommitLinesOfCodeComponent;
  let fixture: ComponentFixture<CommitLinesOfCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommitLinesOfCodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitLinesOfCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
