import { Injectable, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface record {
  name: string;
  week: number;
  day: number;
  hour: number;
  timestamp: number;
  date: string;
  comment: string;
}

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

    public commits: record[] = [];
  
    /**
     * Use Http
     * @param http
     */
    constructor(private http: HttpClient) {
    }

    accumulate(response) {
      response.forEach(commit => {
        this.commits.push({
          name: commit.name, 
          week: Number.parseInt(commit.week),
          day: Number.parseInt(commit.day),
          hour: Number.parseInt(commit.hour),
          timestamp: Number.parseInt(commit.timestamp),
          date: commit.date,
          comment: commit.comment, 
        });
      });
    }
  
    /**
     * Load the data
     *
     * @return {*}
     */
    load() {
      const jsonFile = `assets/comments.json`;
      const that=this;
  
      return new Promise<void>((resolve, reject) => {
        this.http.get(jsonFile).toPromise().then((response) => {
          that.accumulate(response);
          // that.unpack();
          resolve();
        }).catch((response: any) => {
          reject(`Error reading file '${jsonFile}': ${JSON.stringify(response)}`);
        });
      });
    }
  }