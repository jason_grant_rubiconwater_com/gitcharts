import { Component, Input, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';


@Component({
  selector: 'app-commit-lines-of-code',
  templateUrl: './commit-lines-of-code.component.html',
  styleUrls: ['./commit-lines-of-code.component.scss']
})
export class CommitLinesOfCodeComponent implements OnInit {

  echartsInstance;
  chartOption: EChartsOption;

  @Input() committer: string;
  @Input() commits: Map<string, []>;
  @Input() field: number;
  @Input() bubblescale: number;
  @Input() max: number;

  /**
   * Two dimensional array of data: https://echarts.apache.org/en/option.html#series-scatter.data
   *     data: [
        // week   commmits   fileChanged linesAdded linesDeleted
        [  3.4,    4.5,   15,   43],
        [  4.2,    2.3,   20,   91],
        [  10.8,   9.5,   30,   18],
        [  7.2,    8.8,   18,   57]
    ]
   * 
   *
   */
  userCommits:number[][];

  title: string;
  legend: string;
  data: number[];

  constructor() { }

  ngOnInit(): void {
    this.userCommits=this.commits.get(this.committer);
    this.data=this.userCommits[this.field];

    this.title=this.committer;
    this.legend=this.committer;
    this.init();
  }

  ngAfterViewInit(): void {
  }

  init() {
    var that=this;
    this.chartOption = {
      title: {
        text: this.title,
        textAlign: "left",
        textVerticalAlign: "top",
        itemGap: 1,
        top: 10,
        left: 70
      },
      // legend: {
      //   data: [this.legend],
      //   left: 'right',
      // },
      grid: {
        left: 2,
        bottom: 10,
        right: 10,
        top: 10,
        containLabel: true,
      },
      xAxis: {
        type: 'value',
        boundaryGap: false,
        splitLine: {
          show: true,
        },
        axisLine: {
          show: false,
        },
        max: 52,
      },
      yAxis: {
        type: 'value',
        axisLine: {
          show: false,
        },
        max: this.max,

      },
      series: [
        {
          // name: this.legend,
          type: 'line',
          areaStyle: {},
          data: this.data,
          symbolSize: function (val) {
            return val[1] > 0 ? 3 : 0;
          },
        },
      ],
    };
  }

  onChartInit(ec) {
    this.echartsInstance = ec;
  }
}
