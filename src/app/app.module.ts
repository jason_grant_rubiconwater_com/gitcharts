import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { HttpClientModule } from '@angular/common/http';
import { CommitPunchcardComponent } from './commit-punchcard.component';
import { CommitTimelineComponent } from './commit-timeline.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { CommitLinesOfCodeComponent } from './commit-lines-of-code.component';

@NgModule({
  declarations: [
    AppComponent,
    CommitPunchcardComponent,
    CommitTimelineComponent,
    CommitLinesOfCodeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatTabsModule,
    NgxEchartsModule.forRoot({
      /**
       * This will import all modules from echarts.
       * If you only need custom modules,
       * please refer to [Custom Build] section.
       */
      echarts: () => import('echarts'), // or import('./path-to-my-custom-echarts')
    }),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
